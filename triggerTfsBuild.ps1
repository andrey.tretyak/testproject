Write-Host "Triggering TFS build pipeline"

# Settings
$buildId = 7609
$insance = 'tfs.com'
$collection = 'HIH'
$project = 'HIH'

# Rest call logic
$url = 'https://$insance/$collection/$project/_apis/build/builds?api-version=4.1'
$text = "{0}:{1}" -f @("", $Env:K8S_SECRET_TFSPAT)
$encoded = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes($text))
$authHeader = "Basic {0}" -f $encoded

Invoke-RestMethod -Method 'Post' -Uri $url -ContentType "application/json" -Headers @{Authorization = $authHeader} -Body  "{ definition: { id: $buildId }"